skins_file version 1
3
man 0
 man_body man_calf_l m_handL
 male_head 28 skinkey_chin_size 240 0 -0.500000 0.250000 Chin_Size skinkey_chin_shape 230 0 -0.250000 0.250000 Chin_Shape skinkey_chin_forward 250 0 -0.750000 0.750000 Chin_Forward skinkey_jaw_width 130 0 -0.750000 0.500000 Jaw_Width skinkey_lower_lip 120 0 -0.250000 0.250000 Lower_Lip skinkey_upper_lip 110 0 -0.250000 0.250000 Upper_Lip skinkey_mouth_nose_distance 100 0 0.500000 -0.250000 Mouth-Nose_Distance skinkey_mouth_width 90 0 0.500000 -0.500000 Mouth_Width skinkey_nostril_size 30 0 -0.250000 0.250000 Nostril_Size skinkey_nose_height 60 0 0.500000 -0.250000 Nose_Height skinkey_nose_width 40 0 -0.250000 0.500000 Nose_Width skinkey_nose_size 70 0 -0.250000 0.250000 Nose_Size skinkey_nose_shape 50 0 0.500000 -0.250000 Nose_Shape skinkey_nose_bridge 80 0 -0.500000 0.500000 Nose_Bridge skinkey_eye_width 160 0 -0.250000 0.500000 Eye_Width skinkey_eye_to_eye_dist 190 0 -0.750000 0.250000 Eye_to_Eye_Dist skinkey_eye_shape 170 0 -0.750000 0.750000 Eye_Shape skinkey_eye_depth 200 0 -0.500000 0.500000 Eye_Depth skinkey_eyelids 180 0 1.000000 -0.500000 Eyelids skinkey_cheeks 20 0 0.500000 -0.500000 Cheeks skinkey_cheek_bones 260 0 -0.500000 0.750000 Cheek_Bones skinkey_eyebrow_height 220 0 0.500000 -0.500000 Eyebrow_Height skinkey_eyebrow_shape 210 0 -0.250000 0.250000 Eyebrow_Shape skinkey_temple_width 10 0 -0.500000 0.250000 Temple_Width skinkey_face_depth 270 0 -0.500000 0.500000 Face_Depth skinkey_face_ratio 150 0 -0.250000 0.250000 Face_Ratio skinkey_face_width 140 0 -0.750000 0.500000 Face_Width skinkey_post_edit 280 0 1.000000 1.000000 Post-Edit 
22
 man_hair_s  man_hair_m  man_hair_n  man_hair_o  man_hair_y10  man_hair_y12  man_hair_p  man_hair_r  man_hair_q  man_hair_v  man_hair_t  man_hair_y6  man_hair_y3  man_hair_y7  man_hair_y9  man_hair_y11  man_hair_u  man_hair_y  man_hair_y2  man_hair_y4  man_hair_y5  man_hair_y8 
 24
  beard_e
  beard_d
  beard_k
  beard_l
  beard_i
  beard_j
  beard_z
  beard_m
  beard_n
  beard_y
  beard_p
  beard_o
  beard_v
  beard_f
  beard_b
  beard_c
  beard_t
  beard_u
  beard_r
  beard_s
  beard_a
  beard_h
  beard_g
  beard_q

 1  hair_blonde 
 1  beard_blonde 
 24  manface_young_2 4291551456 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_midage 4292866017 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_young 4291879136 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_young_3 4292668909 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_7 4290824392 1 3  hair_blonde  4281545523  4279702291  4278848010  manface_midage_2 4259629272 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_rugged 4289768117 1 2  hair_blonde  4279702291  4278848010  manface_african 4286610570 1 2  hair_blonde  4279702291  4278848010  manface_white2 4292929768 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_mideast1 4289638566 1 3  hair_blonde  4281545523  4279702291  4278848010  manface_mideast2 4291872961 1 3  hair_blonde  4281545523  4279702291  4278848010  manface_black1 4287063388 1 2  hair_blonde  4279702291  4278848010  manface_black2 4284101677 1 2  hair_blonde  4279702291  4278848010  manface_new_young 4291551456 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_new_young_2 4292668909 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_new_young_3 4291879136 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_new_midage 4292866017 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_new_midage_2 4259629272 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_white1 4292929768 1 3  hair_blonde  4294967295  4279702291  4286592294  manface_white3 4292929768 1 3  hair_blonde  4281545523  4279702291  4278848010  manface_new_7 4290824392 1 3  hair_blonde  4281545523  4279702291  4278848010  manface_mideast3 4292929768 1 3  hair_blonde  4281545523  4279702291  4278848010  manface_new_rugged 4289768117 1 2  hair_blonde  4279702291  4278848010  manface_new_african 4286610570 1 2  hair_blonde  4279702291  4278848010 
 7  0 snd_man_die  1 snd_man_hit  2 snd_man_grunt  3 snd_man_grunt_long  4 snd_man_yell  7 snd_man_stun  6 snd_man_victory 
 skel_human 1.000000 
2 3
0

woman 1
 woman_body woman_calf_l f_handL
 female_head 28 skinkey_chin_size 230 0 1.000000 -1.000000 Chin_Size skinkey_chin_shape 220 0 -0.750000 1.000000 Chin_Shape skinkey_chin_forward 10 0 -0.750000 1.000000 Chin_Forward skinkey_jaw_width 20 0 -1.000000 1.000000 Jaw_Width skinkey_jaw_position 40 0 -1.000000 1.000000 Jaw_Position skinkey_mouth_nose_distance 270 0 -1.000000 1.000000 Mouth-Nose_Distance skinkey_mouth_width 30 0 -0.500000 1.000000 Mouth_Width skinkey_cheeks 50 0 -1.000000 1.000000 Cheeks skinkey_nose_height 60 0 -0.500000 0.750000 Nose_Height skinkey_nose_width 70 0 -1.000000 1.250000 Nose_Width skinkey_nose_size 80 0 1.500000 -1.250000 Nose_Size skinkey_nose_shape 240 0 -1.500000 1.500000 Nose_Shape skinkey_nose_bridge 90 0 -1.250000 0.750000 Nose_Bridge skinkey_cheek_bones 100 0 -1.000000 1.250000 Cheek_Bones skinkey_eye_width 150 0 -1.000000 0.500000 Eye_Width skinkey_eye_to_eye_dist 110 0 1.500000 -1.000000 Eye_to_Eye_Dist skinkey_eye_shape 120 0 -0.750000 0.500000 Eye_Shape skinkey_eye_depth 130 0 -0.750000 0.750000 Eye_Depth skinkey_eyelids 140 0 1.000000 -0.500000 Eyelids skinkey_eyebrow_position 160 0 1.000000 -0.750000 Eyebrow_Position skinkey_eyebrow_height 170 0 -0.500000 0.500000 Eyebrow_Height skinkey_eyebrow_depth 250 0 -0.750000 0.750000 Eyebrow_Depth skinkey_eyebrow_shape 180 0 -0.750000 0.500000 Eyebrow_Shape skinkey_temple_width 260 0 1.000000 -1.000000 Temple_Width skinkey_face_depth 200 0 -0.750000 0.750000 Face_Depth skinkey_face_ratio 210 0 -0.750000 0.250000 Face_Ratio skinkey_face_width 190 0 -0.500000 0.500000 Face_Width skinkey_post_edit 280 0 0.000000 1.000000 Post-Edit 
17
 woman_hair_p  woman_hair_n  woman_hair_o  woman_hair_q  woman_hair_r  woman_hair_t  woman_hair_s  sib_curly  longstraight  courthair  man_hair_r  man_hair_p  man_hair_q  man_hair_n  man_hair_s  man_hair_m  man_hair_o 
 14
  acc1
  acc2
  acc3
  acc4
  acc5
  acc6
  acc7
  acc8
  acc9
  acc10
  acc11
  acc12
  acc13
  acc14

 1  hair_blonde 
 1  jewellery 
 10  womanface_young 4293126383 1 3  hair_blonde  4294967295  4279702291  4286592294  womanface_b 4292861919 1 3  hair_blonde  4294967295  4279702291  4286592294  womanface_a 4293451749 1 3  hair_blonde  4281545523  4279702291  4278848010  womanface_brown 4289699710 1 3  hair_blonde  4281545523  4279702291  4278848010  womanface_african 4286611584 1 2  hair_blonde  4279702291  4278848010  womanface_new_young 4293126383 1 3  hair_blonde  4294967295  4279702291  4286592294  womanface_new_b 4292861919 1 3  hair_blonde  4294967295  4279702291  4286592294  womanface_new_a 4293451749 1 3  hair_blonde  4294967295  4279702291  4286592294  womanface_new_brown 4289699710 1 3  hair_blonde  4281545523  4279702291  4278848010  womanface_new_african 4286611584 1 2  hair_blonde  4279702291  4278848010 
 3  0 snd_woman_die  1 snd_woman_hit  4 snd_woman_yell 
 skel_human 1.000000 
2 3
0

dummy 0
 man_body man_calf_l m_handL
 male_head 28 skinkey_chin_size 240 0 -0.500000 0.250000 Chin_Size skinkey_chin_shape 230 0 -0.250000 0.250000 Chin_Shape skinkey_chin_forward 250 0 -0.750000 0.750000 Chin_Forward skinkey_jaw_width 130 0 -0.750000 0.500000 Jaw_Width skinkey_lower_lip 120 0 -0.250000 0.250000 Lower_Lip skinkey_upper_lip 110 0 -0.250000 0.250000 Upper_Lip skinkey_mouth_nose_distance 100 0 0.500000 -0.250000 Mouth-Nose_Distance skinkey_mouth_width 90 0 0.500000 -0.500000 Mouth_Width skinkey_nostril_size 30 0 -0.250000 0.250000 Nostril_Size skinkey_nose_height 60 0 0.500000 -0.250000 Nose_Height skinkey_nose_width 40 0 -0.250000 0.500000 Nose_Width skinkey_nose_size 70 0 -0.250000 0.250000 Nose_Size skinkey_nose_shape 50 0 0.500000 -0.250000 Nose_Shape skinkey_nose_bridge 80 0 -0.500000 0.500000 Nose_Bridge skinkey_eye_width 160 0 -0.250000 0.500000 Eye_Width skinkey_eye_to_eye_dist 190 0 -0.750000 0.250000 Eye_to_Eye_Dist skinkey_eye_shape 170 0 -0.750000 0.750000 Eye_Shape skinkey_eye_depth 200 0 -0.500000 0.500000 Eye_Depth skinkey_eyelids 180 0 1.000000 -0.500000 Eyelids skinkey_cheeks 20 0 0.500000 -0.500000 Cheeks skinkey_cheek_bones 260 0 -0.500000 0.750000 Cheek_Bones skinkey_eyebrow_height 220 0 0.500000 -0.500000 Eyebrow_Height skinkey_eyebrow_shape 210 0 -0.250000 0.250000 Eyebrow_Shape skinkey_temple_width 10 0 -0.500000 0.250000 Temple_Width skinkey_face_depth 270 0 -0.500000 0.500000 Face_Depth skinkey_face_ratio 150 0 -0.250000 0.250000 Face_Ratio skinkey_face_width 140 0 -0.750000 0.500000 Face_Width skinkey_post_edit 280 0 1.000000 1.000000 Post-Edit 
0

 0

 0 
 0 
 0 
 0 
 skel_human 1.000000 
2 3
0

